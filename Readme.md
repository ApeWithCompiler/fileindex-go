# fileindex-go

A findx implementation for golang

Findx is a tiny append only file storage for storing local metadata.
The intended metadata are source urls of the files and fingerprints, but every key value combination can be used.

## Usage

```go
import (
    findx "gitlab.com/ApeWithCompiler/fileindex-go"
)

// Save metadata
err := findx.KeySet(findx.UrlsFile("/tmp/"), "foo.png", "https://example.com/foo.png")

// Delete metadata
err := findx.KeyUnset(findx.UrlsFile("/tmp/"), "foo.png")

// Search metadata
// not found returns err
val, err := findx.KeyGet(findx.UrlsFile("/tmp/"), "foo.png")
```
