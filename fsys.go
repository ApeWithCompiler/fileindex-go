package fileindexgo

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

const (
	FINDX_URLS_FILE string = ".remote.urls"
	FINDX_FINGERPRINTS_FILE string = ".remote.md5sums"
	FINDX_DELIMITER = " "
	FINDX_DELETED = "DEL"
)

func KeySet(pathname, key, value string) error {
	f, err := os.OpenFile(pathname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(fmt.Sprintf("%s%s%s\n", key, FINDX_DELIMITER, value))
	if err != nil {
		return err
	}

	return nil
}

func KeyUnset(pathname, key string) error {
	return KeySet(pathname, key, FINDX_DELETED)
}

func Iter(pathname string, fn func(key, value string)) error {
	f, err := os.OpenFile(pathname, os.O_RDONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), FINDX_DELIMITER)
		if len(s) == 1 {
			fn(s[0], "")
		} else if len(s) == 2 {
			fn(s[0], s[1])
		} else {
			continue
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func KeyGet(pathname, key string) (string, error) {
	value := ""

	f, err := os.OpenFile(pathname, os.O_RDONLY, 0644)
	if err != nil {
		return value, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), FINDX_DELIMITER)
		if len(s) == 2 {
			lvalue := strings.TrimSuffix(s[1], "\n")
			if lvalue != FINDX_DELETED {
				value = lvalue
			} else {
				value = ""
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return value, err
	}

	if value == "" {
		return "", fmt.Errorf("Key not found")
	}

	return value, nil
}

func UrlsFile(pathname string) string {
	return filepath.Join(pathname, FINDX_URLS_FILE)
}

func FingerprintsFile(pathname string) string {
	return filepath.Join(pathname, FINDX_FINGERPRINTS_FILE)
}
